package com.ayg.jacoco.test.demo.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JacocoInfo {
    public JacocoInfo() {
        this.name = "yang";
    }
    public JacocoInfo(String name, String version, String author) {
        this.name = name;
        this.version = version;
        this.author = author;
    }

    private String name;
    private String version;
    private String author;
}
