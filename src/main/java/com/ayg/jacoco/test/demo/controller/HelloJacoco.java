package com.ayg.jacoco.test.demo.controller;

import com.ayg.jacoco.test.demo.entity.JacocoInfo;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloJacoco {
   // @RequestMapping(value = "/hello", method = RequestMethod.POST)
    @RequestMapping(value = "/hello")
    @ResponseBody
    public String test(Model model) {
        model.addAttribute("message","Hello World!!!test");
        System.out.println("test");
        return "hello";
    }
    @RequestMapping("/hello2")
    @ResponseBody
    public String HelloWorld(Model model){
        model.addAttribute("message","Hello World!!!spring");
        return "hello2";
    }

    @RequestMapping("/hello3")
    @ResponseBody
    public JacocoInfo helloJacoco(Model model){
        //return new HelloJacoco("jenny","d","d");
        return new JacocoInfo();
    }
}

